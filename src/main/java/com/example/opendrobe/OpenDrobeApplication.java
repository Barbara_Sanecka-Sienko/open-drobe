package com.example.opendrobe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenDrobeApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenDrobeApplication.class, args);
	}

}
